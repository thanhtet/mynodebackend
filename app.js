const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv/config')

// const http = require('http');
// const server = http.createServer((req, res) => {
//     if(req.url === '/'){
//         console.log("here...");
//         res.write('Hello World');
//         res.end();
//     }
//    if(req.url === '/api/courses'){
//        res.write(JSON.stringify([1, 2, 3]));
//        res.end();
//    }
// })
// server.listen(3900);

// middleware
app.use(cors());
app.use(bodyParser.json());

// middleware
// app.use('/posts', () => {
//     console.log("This is a middleware.");
// })

// import routes
const postsRoute = require('./routes/posts');
const { json } = require('body-parser');

app.use('/posts', postsRoute);

// Routes
app.get('/', (req, res) => {
    res.send("We are on home.");
});

// app.get('/posts', (req, res) => {
//     res.send("We are on posts");
// });

// connect to db
// mongoose.connect(
//     'mongodb+srv://sip:P@ger123@cluster0.pwbbs.mongodb.net/rest?retryWrites=true&w=majority', 
//     { useNewUrlParser: true, useUnifiedTopology: true }, 
//     () => {
//     console.log("database is connected.");
// });

mongoose.connect(
    process.env.DB_CONNECTION, 
    { useNewUrlParser: true, useUnifiedTopology: true }, 
    () => {
    console.log("database is connected.");
});

app.listen(3000, () => {
    console.log("The nodejs server is running on post 3000...");
}); 
